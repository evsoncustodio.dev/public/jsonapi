package dev.evsoncustodio.jsonapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonapiServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonapiServerApplication.class, args);
	}

}
